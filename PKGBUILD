# Maintainer: Future Linux Team <future_linux@163.com>

pkgname=ncurses
pkgver=6.3
pkgrel=2
pkgdesc="System V Release 4.0 curses emulation library"
arch=('x86_64')
url="https://invisible-island.net/ncurses/ncurses.html"
license=('MIT')
depends=('glibc' 'gcc-libs')
makedepends=('autoconf-archive')
optdepends=('bash: for ncursesw6-config')
source=(https://invisible-mirror.net/archives/${pkgname}/${pkgname}-${pkgver}.tar.gz
    ${pkgname}-${pkgver}-libs64.patch
    ${pkgname}-${pkgver}-pkgconfig.patch)
sha256sums=(97fc51ac2b085d4cde31ef4d2c3122c21abc217e9090a43a30fc5ec21684e059
    2e433ff96f0dc89b7763d6d1e1c3a044e625d788400b6eb6b587edd544b045a0
    4c2af64b0f9ae392a9b92facec6524ef6fe8ab479d476cca65b9abb29c6f342a)

prepare() {
    cd ${pkgname}-${pkgver}

    # do not link against test libraries
    patch -Np1 -i ${srcdir}/${pkgname}-${pkgver}-libs64.patch
    # do not leak build-time LDFLAGS into the pkgconfig files:
    # https://bugs.archlinux.org/task/68523
    patch -Np1 -i ${srcdir}/${pkgname}-${pkgver}-pkgconfig.patch
    # NOTE: can't run autoreconf because the autotools setup is custom and ancient
}

build() {
    cd ${pkgname}-${pkgver}

    ${configure}                      \
        --mandir=/usr/share/man       \
        --with-shared                 \
        --without-debug               \
        --without-normal              \
        --enable-pc-files             \
        --enable-widec                \
        --with-cxx-binding            \
        --with-cxx-shared             \
        --with-manpage-format=normal  \
        --with-versioned-syms         \
        --with-xterm-kbs=del          \
        --without-ada                 \
        --with-pkg-config-libdir=/usr/lib64/pkgconfig

    make
}

package() {
    cd ${pkgname}-${pkgver}

    make DESTDIR=${pkgdir} install

    # fool packages looking to link to non-wide-character ncurses libraries
    for lib in ncurses ncurses++ form panel menu; do
        printf "INPUT(-l%sw)\n" ${lib} > ${pkgdir}/usr/lib64/lib${lib}.so
        ln -sv ${lib}w.pc ${pkgdir}/usr/lib64/pkgconfig/${lib}.pc
    done

    # some packages look for -lcurses during build
    printf 'INPUT(-lncursesw)\n' > ${pkgdir}/usr/lib64/libcursesw.so
    ln -sv libncurses.so ${pkgdir}/usr/lib64/libcurses.so

    # tic and ticinfo functionality is built in by default
    # make sure that anything linking against it links against libncursesw.so instead
    for lib in tic tinfo; do
        printf "INPUT(libncursesw.so.%s)\n" ${pkgver%.*} > ${pkgdir}/usr/lib64/lib${lib}.so
        ln -sv libncursesw.so.${pkgver%.*} ${pkgdir}/usr/lib64/lib${lib}.so.${pkgver%.*}
        ln -sv ncursesw.pc ${pkgdir}/usr/lib64/pkgconfig/${lib}.pc
    done
}
